﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseManager.Shared.Enums
{
    public enum Genre
    {
        Os,
        ServerOs,
        Office,
        Multimedia,
        Others,
    }
}
